<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $fillable = ['code', 'name', 'description'];
	public $timestamps = false;

    public function positions()
    {
    	return $this->hasMany(Position::class);
    }
}
