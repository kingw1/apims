<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Auth, Theme;

class AuthController extends Controller
{
    public function login()
    {
        Theme::uses('default')->layout('auth');
    	return $this->theme->of('login')->render();
    }

    public function checkUser(LoginRequest $request)
    {
    	if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
    		return redirect()->intended('dashboard');
    	} else {
            return back();
        }
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }
}
