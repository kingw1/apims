<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class DashboardController extends Controller
{
    public function index(\App\Department $department, \App\Position $position)
    {
    	$departmentIsCreated = $department->all();
    	$positionIsCreated = $position->all();

    	return $this->theme->of('dashboard', compact('departmentIsCreated', 'positionIsCreated'))->render();
    }
}