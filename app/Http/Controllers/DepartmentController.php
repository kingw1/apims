<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\DepartmentRequest;
use App\Department;

class DepartmentController extends Controller
{
    public function index()
    {
    	$departments = Department::all();

    	$this->theme->asset()->container('footer')->usePath()->add('department-script', 'js/app/department.js');
    	return $this->theme->of('department.index', compact('departments'))->render();
    }

    public function create()
    {
    	return $this->theme->of('department.create')->render();
    }

    public function store(DepartmentRequest $request)
    {
    	Department::create($request->all());

		flash()->success(trans('app.save_completed'));

        return redirect('department');
    }

    public function edit(Department $department)
    {
        return $this->theme->of('department.edit', compact('department'))->render();
    }

    public function update($id, Request $request)
    {
        $department = Department::findOrFail($id);
        $department->update($request->all());

        flash()->success(trans('app.save_completed'));

        return redirect('department');
    }
}