<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class PositionController extends Controller
{
    public function index()
    {
    	return $this->theme->of('position.index')->render();
    }
}
