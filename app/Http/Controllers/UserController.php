<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class UserController extends Controller
{
	public function index()
	{
		$users = User::all();

		return $this->theme->of('user.index', compact('users'))->render();
	}

	public function create()
	{
		return $this->theme->of('user.form')->render();
	}

	public function addUser(Request $request, User $user)
	{
		$user->create($request->all());

		return redirect('users');
	}

	public function show(User $user)
	{
		return $this->theme->of('user.profile', compact('user'))->render();
	}
}