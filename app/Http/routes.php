<?php

Route::get('/', 'AuthController@login');
Route::get('login', 'AuthController@login');
Route::post('login', 'AuthController@checkUser');

Route::group(['middleware' => ['auth']], function() {

	Route::get('/', 'DashboardController@index');

	// Dashboard
	Route::get('dashboard', 'DashboardController@index');
    Route::get('logout', 'AuthController@logout');

    // Department
    Route::resource('department', 'DepartmentController');

    // User
	Route::get('users', 'UserController@index');
	Route::get('user/new', 'UserController@create');
	Route::post('user/new', 'UserController@addUser');
	Route::get('user/{user}', 'UserController@show');
});