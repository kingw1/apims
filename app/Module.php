<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function positions()
    {
    	return $this->belongsToMany(Position::class, 'permissions');
    }
}
