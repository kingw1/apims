<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	public function modules()
	{
		return $this->belongsToMany(Module::class, 'permissions');
	}
}