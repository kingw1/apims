function loadModal(url, title, isSubmit, frmName)
{
    $('#myModal .modal-title').html(title);

    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        beforeSend: function(){
            if (isSubmit == false || isSubmit == '' || typeof isSubmit === "undefined") {
                $('#myModal .modal-submit').hide();
            } else if(isSubmit == true) {
                $('#myModal .modal-submit').show();
            }

            if (isSubmit && typeof frmName !== 'undefined') {
                $('form').prop('id', frmName);
                $('#btn-submit').attr('onclick', 'return '+frmName+'(this);');
            }

            $('#myModal .modal-body').html('<i class="fa fa-spinner fa-lg" aria-hidden="true"></i>');
        },
        success: function(data){
            $('#myModal .modal-body').html(data);
        },
        error: function(xhr, status, error) {
            alert('Error !');
        }
    });
}

function formInputValidate(inputs)
{
    var result = [];
    $.each(inputs, function(index, inputName){
        var inputVal = $('input[name="'+inputName+'"]').val();
        if (inputVal == 0 || inputVal == '') {
            var label = $('label[for="'+inputName+'"]').html();
            $('input[name="'+inputName+'"]').focus();
            $('input[name="'+inputName+'"]').parent().parent().addClass('has-error');
            result.push(1);
            return false;
        }
    });

    return result;
}

function formSelectValidate(inputs)
{
    var result = [];
    $.each(inputs, function(index, inputName){
        var inputVal = $('select[name="'+inputName+'"]').val();
        if (inputVal == 0 || inputVal == '') {
            var label = $('label[for="'+inputName+'"]').html();
            $('select[name="'+inputName+'"]').focus();
            $('select[name="'+inputName+'"]').parent().parent().addClass('has-error');
            result.push(1);
            return false;
        }
    });

    return result;
}

function disabledSubmit()
{
    $('input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    $('button[type="button"]').prop('disabled', true);
}

function undisabledSubmit()
{
    $('input[type="submit"]').prop('disabled', false);
    $('button[type="submit"]').prop('disabled', false);
    $('button[type="button"]').prop('disabled', false);
}