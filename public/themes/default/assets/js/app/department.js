$(function(){

});

var frm = 'frmDepartment';
function createNewDepartment()
{
	var btn = $('#modal-create-new-department');
	var url = btn.data('href');
	var title = btn.data('title');

	loadModal(url, title, true, frm);
}

function frmDepartment()
{
	$.ajax({
		url: BASE_URL+'department/new',
		data: $('#'+frm).serialize(),
		method: 'POST',
		dataType: 'JSON',
		cache: false,
		beforeSend: function(){
			disabledSubmit();
		},
		success: function(data){
			alert(data.message);
			undisabledSubmit();
		},
		error: function(data){
			var errors = $.parseJSON(data.responseText);
			$.each(errors, function(name, val){
				$('#'+name).parent().parent().addClass('has-error');
				$('#'+name).parent().append('<span class="text-danger">'+val[0]+'</span>');
			});

			undisabledSubmit();
		}
	});
}