<?php

return array(
    'inherit' => null, //default
    'events' => array(

        'before' => function($theme) {

            $theme->setTitle('AP - Inventory Management System');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        'beforeRenderTheme' => function($theme) {
            $theme->asset()->usePath()->add('core-css', 'bootstrap/css/bootstrap.min.css');
            $theme->asset()->usePath()->add('core-font-css', 'fonts/rsu/stylesheet.css');
            $theme->asset()->usePath()->add('core-icon-css', 'font-awesome/css/font-awesome.min.css');
            $theme->asset()->usePath()->add('jquery', 'js/jquery-2.1.1.min.js');
        },

        'beforeRenderLayout' => array(
            'auth' => function($theme) {
                $theme->asset()->usePath()->add('app-css', 'css/login.css');
            },

            'default' => function($theme) {
                $theme->asset()->usePath()->add('app-css', 'css/app.css');
                $theme->asset()->usePath()->add('theme-css', 'css/theme.css');
                $theme->asset()->usePath()->add('core-js', 'bootstrap/js/bootstrap.min.js');
                $theme->asset()->container('footer')->usePath()->add('init-js', 'js/init.js');
                $theme->asset()->container('footer')->usePath()->add('app-js', 'js/app.js');
                $theme->asset()->container('footer')->usePath()->add('form-js', 'js/form.min.js');
            },
        )

    )

);