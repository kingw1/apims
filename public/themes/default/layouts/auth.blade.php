<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::asset()->styles() !!}
        {!! Theme::asset()->scripts() !!}
    </head>
    <body>

        {!! Theme::partial('header-admin') !!}

        <div id="login">{!! Theme::content() !!}</div>

        {!! Theme::partial('footer-admin') !!}

        {!! Theme::asset()->container('footer')->scripts() !!}
    </body>
</html>