<!DOCTYPE html>
<html lang="th">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::asset()->styles() !!}
        {!! Theme::asset()->scripts() !!}
    </head>
    <body>
        {!! Theme::partial('header') !!}

        <div class="container">

            <div style="padding-bottom: 5px;">
            @include('flash::message')
            </div>

            {!! Theme::content() !!}

        </div>

        {!! Theme::partial('footer') !!}

        {!! Theme::partial('modal') !!}

        <script>
            var BASE_URL = '{{url('/')}}/';
            var AJAX_LOAD_ERROR = '@lang('app.ajax_load_error')';
        </script>

        {!! Theme::asset()->container('footer')->scripts() !!}
    </body>
</html>