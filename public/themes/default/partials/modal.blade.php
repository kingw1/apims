<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="frm-modal" class="form-horizontal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">...</h4>
            </div>
            <div class="modal-body">...</div>
            <div class="modal-footer">
                <div class="pull-left modal-submit">
                    <button type="button" class="btn btn-primary" id="btn-submit">@lang('app.btn-submit')</button>
                </div>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.btn-modal-close')</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>