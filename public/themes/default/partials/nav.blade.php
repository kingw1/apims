<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}">@lang('app.home')</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('app.setting') <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <!-- <li><a href="{{ url('user') }}">@lang('setting.hr')</a></li>
                <li><a href="{{ url('customer') }}">@lang('setting.customer')</a></li>
                <li><a href="{{ url('supplier') }}">@lang('setting.supplier')</a></li>
                <li class="divider"></li>
                <li><a href="{{ url('organization') }}">@lang('setting.organization')</a></li> -->
                <li><a href="{{ url('department') }}">@lang('setting.department')</a></li>
                <li><a href="{{ url('position') }}">@lang('setting.position')</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i> {{ Auth::user()->profile->firstname }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#">@lang('user.profile')</a></li>
                <li><a href="#">@lang('user.edit_password')</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> @lang('app.logout')</a></li>
            </ul>
        </li>
    </ul>
</div>