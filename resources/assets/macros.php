<?php

/**
 * Form Button
 * @var string
 */
Form::macro('btn', function($url, $name, $title = ''){
	$title = empty($title) ? $name : $title;
	$html = '<a href="'.$url.'" class="btn btn-default" title="'.$title.'">'.$name.'</a>';
	return $html;
});

Form::macro('btnEdit', function($url, $title = ''){
	$title = empty($title) ? $name : $title;
	$html = '<a href="'.$url.'" class="btn btn-default" title="'.$title.'">'.trans('app.edit').'</a>';
	return $html;
});

Form::macro('btnDelete', function($url, $title = ''){
	$title = empty($title) ? $name : $title;
	$html = '<a href="'.$url.'" class="btn btn-danger" title="'.$title.'">'.trans('app.delete').'</a>';
	return $html;
});