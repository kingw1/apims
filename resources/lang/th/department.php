<?php

return [
	'department' => 'แผนก/ฝ่าย',
	'create-new-department' => 'สร้างแผนก/ฝ่าย',
	'edit-department' => 'แก้ไขข้อมูลแผนก/ฝ่าย',
	'delete-department' => 'ลบข้อมูลแผนก/ฝ่าย',
	'code' => 'รหัสแผนก/ฝ่าย',
	'name' => 'ชื่อแผนก/ฝ่าย',
	'description' => 'รายละเอียด',
];