<?php

return [
	'position' => 'ตำแหน่ง',
	'create-new-position' => 'สร้างตำแหน่ง',
	'update-position' => 'แก้ไขข้อมูลตำแหน่ง',
	'delete-position' => 'ลบข้อมูลตำแหน่ง',
];