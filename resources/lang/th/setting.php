<?php

return [
	'organization' => 'ข้อมูลองค์กร',
	'hr' => 'บุคลากร',
	'customer' => 'ลูกค้า',
	'supplier' => 'คู่ค้า',
	'department' => 'แผนก/ฝ่าย',
	'position' => 'ตำแหน่ง',
];