<?php

return [
	'user' => 'บุคคลากร',
	'user-title' => 'จัดการข้อมูลผู้ใช้งาน',
	'profile' => 'ข้อมูลส่วนตัว',
	'edit_password' => 'แก้ไขรหัสผ่าน',
];