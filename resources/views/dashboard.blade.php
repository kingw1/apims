<blockquote>
	<p>@lang('app.welcome') <span class="text-primary"><a href="#">{{ Auth::user()->profile->firstname }}</a></span></p>
	<cite>@lang('app.last_activity') {{ Auth::user()->last_activity }}</cite>
</blockquote>

@if (count($departmentIsCreated) || count($positionIsCreated))

	<div class="alert alert-danger alert-important"><i class="fa fa-exclamation-triangle"></i> เริ่มต้นใช้งานระบบด้วยการตั้งค่าพื้นฐาน โดยการคลิกที่เมนู <u>ตั้งค่าระบบ</u></div>
@endif

<div class="jumbotron dashboard-company">
	<div class="row">
		<div class="col-xs-12 col-md-3">
			<img src="{{ Theme::asset()->url('img/brand.png') }}" class="img-thumbnail" width="200" height="200">
		</div>
		<div class="col-xs-12 col-md-9">
			<h1>AP ELECTRIC</h1>
			<h4>ศูนย์รวมอุปกรณ์ไฟฟ้าทุกชนิด จำหน่ายปลีกส่ง (ทั่วประเทศ)</h4>
		</div>
	</div>
</div>