<div class="page-header">
	<h1>
		@lang('department.create-new-department')
	</h1>
</div>

{!! Form::open(['class' => 'form-horizontal', 'url' => 'department'])  !!}

	@include('department.form')

{!! Form::close() !!}