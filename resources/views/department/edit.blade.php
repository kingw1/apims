<div class="page-header">
	<h1>
		@lang('department.edit-department')
	</h1>
</div>

{!! Form::model($department, ['method' => 'PATCH', 'route' => ['department.update', $department->id], 'class' => 'form-horizontal']) !!}
	@include('department.form')
{!! Form::close() !!}