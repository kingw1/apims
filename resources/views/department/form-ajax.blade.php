<div class="form-group">
	<label for="code" class="control-label col-sm-12 col-md-3">@lang('department.code')</label>
	<div class="col-sm-12 col-md-5">
		<input type="text" name="code" id="code" class="form-control">
	</div>
</div>

<div class="form-group">
	<label for="name" class="control-label col-sm-12 col-md-3">@lang('department.name')</label>
	<div class="col-sm-12 col-md-5">
		<input type="text" name="name" id="name" class="form-control">
	</div>
</div>

<div class="form-group">
	<label for="description" class="control-label col-sm-12 col-md-3">@lang('department.description')</label>
	<div class="col-sm-12 col-md-7">
		<textarea name="description" id="description" rows="3" class="form-control"></textarea>
	</div>
</div>

{{ csrf_field() }}