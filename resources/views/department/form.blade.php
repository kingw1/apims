<div class="form-group {{$errors->first('name') ? 'has-error' : ''}}">
	{!! Form::label('name', trans('department.name'), ['class' => 'control-label col-sm-12 col-md-3']) !!}
	<div class="col-sm-12 col-md-5">
		{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}

		@if ($errors->first('name'))
		<var class="text-danger">{{ $errors->first('name') }}</var>
		@endif
	</div>
</div>

<div class="form-group {{$errors->first('code') ? 'has-error' : ''}}">
	{!! Form::label('code', trans('department.code'), ['class' => 'control-label col-sm-12 col-md-3']) !!}
	<div class="col-sm-12 col-md-5">
		{!! Form::text('code', old('code'), ['class' => 'form-control']) !!}

		@if ($errors->first('name'))
		<var class="text-danger">{{ $errors->first('code') }}</var>
		@endif
	</div>
</div>

<div class="form-group">
	<label for="description" class="control-label col-sm-12 col-md-3">@lang('department.description')</label>
	<div class="col-sm-12 col-md-5">
		{!! Form::textarea('description', old('description'), ['class' => 'form-control', 'rows' => 5]) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12 col-md3 col-md-offset-5">
		<input type="submit" value="@lang('app.btn-submit')" class="btn btn-primary">
	</div>
</div>