<div class="page-header">
	<h1>
		@lang('department.department')

		<div class="pull-right">
			<!-- <a
				onclick="createNewDepartment();"
				data-href="{{url('department/new')}}"
				id="modal-create-new-department"
				class="btn btn-default"
				data-toggle="modal"
				data-target="#myModal"
				data-title="@lang('department.create-new-department')">
				@lang('department.create-new-department')
			</a> -->
			<a href="{{url('department/new')}}" class="btn btn-default">@lang('department.create-new-department')</a>
		</div>
	</h1>
</div>

@if (session('message'))
<div class="alert alert-success">
    {{ session('message') }}
</div>
@endif

<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th width="10%">@lang('department.code')</th>
			<th width="20%">@lang('department.name')</th>
			<th>@lang('department.description')</th>
			<th width="20%">@lang('app.action')</th>
		</tr>
	</thead>
	<tbody>
		@if (count($departments))
		@foreach ($departments as $department)
		<tr>
			<td>{{ $department->code }}</td>
			<td>{{ $department->name }}</td>
			<td>{{ $department->description }}</td>
			<td>
				<a href="{{url("department/$department->id/update")}}" class="btn btn-default" title="@lang('department.update-department')">@lang('app.edit')</a>
				<a href="{{url("department/$department->id/delete")}}" class="btn btn-danger" title="@lang('department.delete-department')">@lang('app.delete')</a>
			</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>