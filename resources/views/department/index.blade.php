<div class="page-header">
	<h1>
		@lang('department.department')

		<div class="pull-right">
			{{ Html::linkRoute('department.create', trans('department.create-new-department'), [], ['class' => 'btn btn-default']) }}
		</div>
	</h1>
</div>

<div class="table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th width="20%">@lang('department.name')</th>
				<th>@lang('department.description')</th>
				<th width="15%">@lang('department.code')</th>
				<th width="15%">@lang('app.action')</th>
			</tr>
		</thead>
		<tbody>
			@if (count($departments))
			@foreach ($departments as $department)
			<tr>
				<td>{{ $department->name }}</td>
				<td>{{ $department->description }}</td>
				<td>{{ $department->code }}</td>
				<td>
					{!! Html::linkRoute('department.edit', trans('app.edit'), [$department->id], ['class' => 'btn btn-default']) !!}
					{!! Html::linkRoute('department.destroy', trans('app.delete'), [$department->id], ['class' => 'btn btn-danger']) !!}

					<!-- <a class="btn btn-danger" href="{{ route('department.destroy', $department->id) }}" data-method="delete" data-confirm="Are you sure ! you want to delete this ?" rel="nofollow">ลบ</a> -->
				</td>
			</tr>
			@endforeach
			@endif
		</tbody>
	</table>
</div>