
<div class="company">
	<div class="logo"><img src="{{ Theme::asset()->url('img/brand.png') }}" class="img-thumbnail"></div>
	<div class="name">AP Electric</div>
	<div class="subname">ศูนย์รวมอุปกรณ์ไฟฟ้าทุกชนิด จำหน่ายปลีกส่ง (ทั่วประเทศ)</div>
</div>

<form method="POST" action="{{ url('login') }}" class="form-signin">
	{{ csrf_field() }}
	<div class="form-group">
		<input type="text" name="username" class="form-control" placeholder="@lang('app.username')">

		@if ($errors->first('username'))
		<span class="error">{{ $errors->first('username') }}</span>
		@endif
	</div>

	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="@lang('app.password')">

		@if ($errors->first('password'))
		<span class="error">{{ $errors->first('password') }}</span>
		@endif
	</div>

	<div class="form-group">
		<button class="btn btn-default btn-block" type="submit">@lang('app.btn-login')</button>
	</div>
</form>