<blockquote class="panel-header">
	<h5>
		จัดการผู้ใช้ <small class="blue-grey-text">เพิ่มผู้ใช้</small>
	</h5>
</blockquote>

<div class="row">
	<form class="col s12" method="POST">
		<div class="row">
			<div class="input-field col s12">
				<input id="name" name="name" type="text" class="validate" placeholder="ใส่ชื่อของคุณ">
				<label for="name">Your Name</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="email" name="email" type="email" class="validate">
				<label for="email">Email</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="password" name="password" type="password" class="validate">
				<label for="password">Password</label>
			</div>
		</div>

		{{ csrf_field() }}
		<button class="btn waves-effect waves-light" type="submit" name="action">@lang('app.btn-submit')</button>
	</form>
</div>