<blockquote class="panel-header">
	<h5>
		@lang('user.user')

		<div class="right panel-menu">
			<a href="{{ url('user/new') }}" class="blue-grey waves-effect waves-light btn">เพิ่มผู้ใช้</a>
			<!-- <button data-target="my-modal" class="btn modal-trigger">Modal</button> -->
		</div>
	</h5>
</blockquote>

<div class="card-panel">
	<table class="responsive-table striped bordered highlight">
		<thead>
			<tr>
				<th>Name</th>
				<th width="20%">@lang('app.action')</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td>{{ $user->profile->firstname }} {{ $user->profile->lastname }}</td>
				<td>
					<button type="button" class="btn waves-effect waves-light blue" title="">@lang('app.edit')</button>
					<button type="button" class="btn waves-effect waves-light red" title="">@lang('app.delete')</button>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>